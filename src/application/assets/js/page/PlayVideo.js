const { ipcRenderer } = require('electron');

class PlayVideo {
    constructor() {
        this.downVideo()
        this.initSwiper()
        this.SwiperIndex = 0
    }

    initSwiper() {
        let self = this;
        new Swiper ('.swiper-container', {
            direction: 'horizontal', // 垂直切换选项
            loop: false, // 循环模式选项
            // 如果需要分页器
            pagination: {
                el: '.swiper-pagination',
            },

            // 如果需要前进后退按钮
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            on: {
                slideChange: function () {
                    self.SwiperIndex = this.realIndex;
                },
            }
        })
    }

    downVideo() {
        let self = this;
        $(".down_video").click(function () {
            // 当前播放的是图片
            if ($("#my-video").length == 0) {
                let allImg = $(".swiper-wrapper .swiper-slide img").eq(self.SwiperIndex)
                ipcRenderer.send('SaveFile', {
                    url: allImg.attr("src")
                })
            // 当前播放的是视频
            } else {
                console.log("下载视频")
                ipcRenderer.send('SaveFile', {
                   url: $("#my-video").attr("src")
                })
            }
        })
    }
}

new PlayVideo();
