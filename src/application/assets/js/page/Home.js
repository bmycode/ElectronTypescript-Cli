const { ipcRenderer } = require('electron');

class Home {
    constructor() {
        this.playVideo()
        this.GotoList()
        this.page = $(".video_list").attr("page")
        this.loadMore()
    }

    /**
     * 点击播放视频，发送ipc通知主进程创建独立的 PlayVideo 窗口
     * 然后向 PlayVideo 方法传入 datat 参数
     */
    playVideo() {
        $(".abbreviation_f").off("click").on('click',function (e) {
            if ($(this).attr("workType") == "video") {
                ipcRenderer.send('openWindow', {
                    action: 'Home.controller/PlayVideo',
                    data: {
                        principalId: $(this).attr("principalId"),
                        photoId: $(this).attr("photoId")
                    }
                });
            } else {
                ipcRenderer.send('openWindow', {
                    action: 'Home.controller/PlayVideo',
                    data: {
                        caption: $(this).next().text(),
                        list: JSON.parse( $(this).find(".imgUrls").text() )
                    }
                });
            }

            return false;
        });
    }

    /**
     * 点击使用默认浏览器打开主播主页
     * @constructor
     */
    GotoList() {
        $(".info").off("click").on('click',function (e) {
            ipcRenderer.send('openExternal', {
                url: `https://live.kuaishou.com/profile/${$(this).prev().attr("principalId")}`
            })
        })
    }

    /**
     * 滚动加载更多数据
     */
    loadMore() {
        let self = this;
        $(window).scroll(function () {
            let scrollTop = $(this).scrollTop();
            let scrollHeight = $(document).height();
            let windowHeight = $(this).height();
            if (scrollTop + windowHeight == scrollHeight) {
                self.getVideoList()
            }
        });
    }

    /**
     * 滚动到底部后发送请求获取数据
     */
    getVideoList() {
        Http("/list", { page: this.page }, res=> {
            let MMList = res.data;

            // 更新页面上的page页码
            $(".video_list").attr({ page: MMList.pcursor })
            // 保存页面到变量
            this.page = $(".video_list").attr("page")
            // 遍历新数据到页面
            MMList.list.forEach(function (val,index) {
                $(".video_list").append(`
                  <li> 
                       <div class="abbreviation_f" principalId="${val.user.id}" photoId="${val.id}" workType="${val.workType}"> 
                          <div style="background:url('${val.thumbnailUrl}');background-position: center center;" class="zz"></div>
                          <img src="${val.thumbnailUrl}" class="thumbnailUrl" /> 
                          <div class="${val.workType == 'video' ? 'tips red':'tips green'}">
                            ${val.workType == 'video' ? '视频':'图片'}
                            ${
                                val.imgUrls.length != 0
                                    ? `<div class="imgUrls" style="display:none">${JSON.stringify(val.imgUrls)}</div>`
                                    : ''
                             }
                          </div>
                       </div> 
                       <div class="info">
                          <img src="${val.user.avatar}" /> 
                          <p>${val.caption}</p> 
                       </div> 
                   </li>
                `);
            });

            this.playVideo()
            this.GotoList()
            // 因为页面数据变多了，所以为所有的dom重新监听点击播放，进入主页事件
        })
    }
}

new Home();
