import { ipcMain } from "electron";
import { Dialog } from "@interactive/Dialog.interactive";
import {Inject, Injectable } from "@annotation/Ioc.annotation";
import { Http } from "@net/Http.net";
import Utils from "@utils/Index.utils";

export class FileIpc {

    @Inject()
    private readonly _Net!: Http;

    openFile() {
        ipcMain.on('SaveFile', async (event, arg) => {
            let SaveFilePath = await Dialog.showSaveDialog("选择路径","选择下载路径");
            if (SaveFilePath != undefined) {
                Utils.DownFile(arg.url,SaveFilePath)
            }
        })
    }
}
