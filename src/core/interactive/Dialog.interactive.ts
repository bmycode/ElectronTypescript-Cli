import { dialog, Notification } from "electron";
import Windows from "@model/Windows.model"
export class Dialog {

    /**
     * 错误弹窗
     * @param title
     * @param content
     * @constructor
     */
    public static async ErrorBox(title: string, content: string): Promise<void> {
        await dialog.showErrorBox(title, content)
    }

    public static async showSaveDialog(title: string, message: string): Promise<any> {
        let SaveFile = await dialog.showOpenDialog(Windows.CurrentBrowserWindow, {
            title: title,
            message: message,
            buttonLabel: '亲，点我确认选择！',
            filters: [
                { name: 'All', extensions: ['*'] },
            ],
            properties: [
                'openDirectory',
                'createDirectory'
            ]
        });

        if(!SaveFile.canceled) {
            return SaveFile.filePaths
        }
    }
}
