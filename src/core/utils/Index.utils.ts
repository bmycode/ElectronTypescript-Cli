import { join } from "path";
import Config from "@config/Index.config";
import {BrowserWindow, Notification, NotificationConstructorOptions, shell} from "electron";
import {mkdirSync, writeFileSync, existsSync, PathLike} from "fs";
import {renderFile} from "jade";
import Windows from "@model/Windows.model";

export default class Utils {

    public static CheckAjaxUrl(): string {
        return Config.ApiUrl.BaseUrl
    }

    /**
     * 返回相对路径
     * @param path 路径
     * @constructor
     */
    public static GetFilePath(path: string): string {
        return join(__dirname, path)
    }

    /**
     * 根据配置返回对应的controller类，require后的类
     * @constructor
     */
    public static GetController(): any {
        return require(`../controller/${(Config.StartPage.split("/"))[0]}.js`)
    }

    /**
     * Home.controller 拆分为数组，然后controller的首字母C大写
     * 然后返回 HomeController 的类名
     */
    public static toUpperCase(cont: string): string {
        let Controller =  cont.split(".")
        return Controller[0]+ Controller[1].charAt(0).toUpperCase() + Controller[1].slice(1);
    }

    /**
     * 系统通知
     * @param parmas
     * @constructor
     */
    public static Notification(parmas: NotificationConstructorOptions): void {
        new Notification(parmas).show();
    }

    /**
     * 下载资源
     * @param url 需要下载的资源地址
     * @param path 下载后需要保存的路径
     * @constructor
     */
    public static DownFile(url: string, path: PathLike) {
        Windows.CurrentBrowserWindow.webContents.downloadURL(url)
        Windows.CurrentBrowserWindow.webContents.session.on(
            "will-download",
            (event: any, item: any, webContents: any) => {
                item.setSavePath(`${path}/${item.getFilename()}`);
                item.once('done', (event: any, state: any) => {
                    if (state === 'completed') {
                        // 下载成功后显示通知
                        Utils.Notification({
                            title: '下载完成',
                            body: `您的视频 ${item.getFilename()} 已成功下载！`,
                            silent: true,
                        })
                    }
                })
            }
        )
    }

    /**
     * 创建文件夹
     * @param name 文件夹
     * @param html html
     */
    public static mkdir(name: string, html: string): void {
        let path = join(__dirname, `../../application/page/${name}`);
        existsSync(path) ? null :  mkdirSync(path)
        Utils.mkFile(path, name, html)
    }

    /**
     * 生成文件并写入数据
     * @param path 路径
     * @param name html文件名
     * @param html html数据
     */
    public static mkFile(path: string, name: string, html: string): void {
        writeFileSync(`${path}/${name}.html`, html)
    }

    /**
     * 创建启动窗口
     * @param target
     * @param name
     */
    public static async startWindows(target: any, name: string, params?: object) {
        let data: { [ p: string ]: any } = await target[name](params),
            Dom: string = renderFile(
                Utils.GetFilePath(`../../../src/application/page/${name}/${name}.jade`),
                Object.assign(Config.jadeCompile0ptions, data),
            );
        // 在dist/ 创建并生成对应的html文件
        Utils.mkdir(name, Dom)
        try {
            // @ts-ignore
            Windows.CurrentBrowserWindow = new BrowserWindow(Config.PageSize[name]);
            // @ts-ignore
            Windows.CurrentBrowserWindow.loadFile(<string>Config.PagePath[name]).then(r => {});
            Windows.CurrentBrowserWindow.show();
        } catch (e) {
            throw new Error("创建窗体失败，请检查配置文件，窗体的html路径是否正确！")
        }
    }
}
