import { BrowserWindowConstructorOptions } from "electron";

export interface PagePath {
    Home?: String,
    PlayVideo?: String
}

export interface PageSize {
    // 系统首页 窗体的配置
    Home?: BrowserWindowConstructorOptions,
    // 设置页面 窗体的配置
    PlayVideo?: BrowserWindowConstructorOptions
}
