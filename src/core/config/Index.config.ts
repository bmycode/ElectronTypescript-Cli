import { PagePath, PageSize } from "@type/PageConfig.types";
import { TrayConfig } from "@type/Tray.types";
import { ApiUrlConfig } from "@type/ApiConfig";
import { app, Menu, shell, MenuItemConstructorOptions, MenuItem, TouchBarConstructorOptions, TouchBar } from "electron";
import Windows from "@/core/model/Windows.model";
const { TouchBarLabel, TouchBarButton,TouchBarSpacer } = TouchBar;
import { join } from "path";
import { JadeOptions } from "jade";

export default class Config {
    /**
     * 启动页面默认为 Home.controller/Home
     * 程序内部根据这个配置自动载入Home.controller.ts文件，并自动调用类里面的Home()方法实现窗口创建
     * 可以改成其他，例如 PlayVideo.controller/Theme
     */
    public static StartPage: string = 'Home.controller/Home';

    // 应用版本号
    public static CurrentAppVersion: String = '0.0.1';

    // 是否为Mac系统
    public static isMac: Boolean = process.platform === 'darwin';

    public static LogsPath: string = "../../../logs/";

    // ajax 请求地址
    public static ApiUrl: ApiUrlConfig = {
        BaseUrl: 'http://www.bmycode.com:3000/api',
        ApiList: {
            PlayList: '/list', // 获取视频列表
            PalyVideo: '/play', // 获取视频播放地址
        }
    };

    // jade 模板引擎配置，更多参数自行阅读声明文件
    public static jadeCompile0ptions: JadeOptions = {
        pretty: true, // 编译输出后是否保留源码格式，true 保持
        globals: {
            css: [
                'http://mdui-aliyun.cdn.w3cbus.com/source/dist/css/mdui.min.css',
                'http://at.alicdn.com/t/font_1934749_sryayjjvf6.css',
            ],
            js: [
                'http://cdn.bootcdn.net/ajax/libs/jquery/3.5.1/jquery.js',
                'http://mdui-aliyun.cdn.w3cbus.com/source/dist/js/mdui.min.js'
            ]
        }
    };

    // 应用程序的页面地址，地址为打包后的相对路径
    public static PagePath: PagePath = {
        Home: join(__dirname, '../../application/page/Home/Home.html'),
        PlayVideo: join(__dirname, '../../application/page/PlayVideo/PlayVideo.html'),
    };

    // 所有页面窗体配置
    public static PageSize: PageSize = {
        Home: {
            width: 1200,
            height: 760,
            frame: false,
            backgroundColor: '#fff',
            titleBarStyle: 'hiddenInset',
            transparent: true,
            webPreferences: {
                webSecurity: false,
                nodeIntegration: true,
                webviewTag: true,
            }
        },
        PlayVideo: {
            width: 1084,
            height: 610,
            frame: false,
            backgroundColor: '#000',
            titleBarStyle: 'hidden',
            modal: true,
            show: false,
            resizable: true,
            transparent: true,
            webPreferences: {
                webSecurity: false,
                nodeIntegration: true,
                webviewTag: true
            }
        }
    };

    // Mac系统顶部全局菜单 右边图标
    public static TrayConfig: TrayConfig = {
        TopMenuRightImage: join(__dirname, '../../application/assets/img/pug.png'),
        TopMenuRightDropdown:[
            {
                label: '显示主窗口',
                click: ()=> {
                    Windows.CurrentBrowserWindow.show();
                }
            },
            {
                icon: join(__dirname, '../../application/assets/img/pug.png'),
                label: '下拉菜单测试',
                type: 'checkbox',
                checked: true,
                click: (menuItem: any, browserWindow: any)=> {
                    console.log("menuItem: ", menuItem);
                }
            },
            {
                label: '菜单',
                submenu: [
                    {
                        label: '子菜单1'
                    },
                    {
                        label: '子菜单2'
                    }
                ],
            },
            {
                role: 'quit',
                label: '退出'
            },
        ],
        TopMenuRightTips: '测试提醒'
    };

    // Mac or win 系统顶部全局菜单
    public static TemplateMenu: Array<(MenuItemConstructorOptions) | (MenuItem)>= [
        {
            label: app.name,
            submenu: [
                { label: `关于 ${app.name}`, role: 'about' },
                { type: 'separator' },
                { label: '服务', role: 'services' },
                { type: 'separator' },
                { label: `隐藏 ${app.name}`, role: 'hide' },
                { role: 'hideOthers' },
                { label: '隐藏其他', role: 'unhide' },
                { type: 'separator' },
                { label: `退出${app.name}`, role: 'quit' }
            ]
        },
        {
            label: '文件',
        },
        {
            label: '编辑',
            submenu: [
                { label: '撤销', role: 'undo' },
                { label: '恢复', role: 'redo' },
                {type: 'separator' },
                { label: '剪切', role: 'cut' },
                { label: '复制', role: 'copy' },
                { label: '粘贴', role: 'paste' },
                {type: 'separator' },
                { label: '粘贴保留样式', role: 'pasteAndMatchStyle' },
                { label: '删除', role: 'delete' },
                { label: '全选', role: 'selectAll' },
                { type: 'separator' },
                {
                    label: '听写',
                    submenu: [
                        { label: '开始听写', role: 'startSpeaking' },
                        { label: '停止听写', role: 'stopSpeaking' }
                    ]
                }
            ]
        },
        {
            label: '视图',
            submenu: [
                { label: '刷新', role: 'reload' },
                { label: '重置', role: 'resetZoom' },
                { label: '放大', role: 'zoomIn' },
                { label: '缩小', role: 'zoomOut' },
                { type: 'separator' },
                { label: '全屏', role: 'togglefullscreen' },
                { label: '切换开发人员工具', role: 'toggleDevTools' },
            ]
        },
        {
            label: '窗口',
            submenu: [
                { label: '最小化', role: 'minimize' },
                { label: '最大化',role: 'zoom' },
                { label: '关闭', role: 'close' }
            ]
        },
        {
            label: '帮助',
            role: 'help',
            submenu: [
                {
                    label: '了解更多',
                    click: async () => {
                        await shell.openExternal('https://electronjs.org')
                    }
                },
                {
                    label: 'GitHub主页',
                    click: async () => {
                    }
                }
            ]
        }
    ];

    // Mac touchbar 菜单
    public static TouchBarConfig: TouchBarConstructorOptions = {
        items: [
            new TouchBarSpacer({ size: 'small' }),
            new TouchBarButton({
                label: '测试1',
                backgroundColor: '#3a3a3c',
                click: () => {}
            }),
            new TouchBarSpacer({ size: 'small' }),
            new TouchBarButton({
                label: '测试2',
                backgroundColor: '#3a3a3c',
                click: () => {}
            }),
        ]
    };
}
