import { Render, Ipc } from "@annotation/Creted.annotation";
import { VideoImplService } from "@service/impl/Video.impl.service";
import { Inject } from "@annotation/Ioc.annotation";
import { FileIpc } from "@ipc/module/File.ipc";

export class HomeController {

    @Inject()
    public readonly VideoImplService!: VideoImplService;

    @Render()
    public async Home(test: string) {
        let res = await this.VideoImplService.PlayList({});
        return {
            header: true,
            nav: true,
            title: '首页页面窗口-测试传给模板的数据',
            desc: '点我发送ipc，打开窗口，代码实现分别在：src/application/assets/js/page/Home.js 和 src/core/ipc/Application.ipc.ts 和 @CreateApplicationIpc 装饰器中！！！',
            list: res.data
        };
    }

    @Render()
    @Ipc([ FileIpc ])
    // 注意 PlayVideo() 方法将被前端发起的全局ipc自动调用，ipc内部会根据这个方法名去自动创建同名的 视频播放窗口，
    // 并IPC会自动动调用这个 PlayVideo() 方法，并把前端的参数向传入方法的 params，方法内部无脑的去用即可！！
    public async PlayVideo(params?: object) {
        return (params as object).hasOwnProperty("list")
        ? { // 用户点击图片组
            header: false,
            nav: false,
            img: true,
            video: params
        }
        : { // 用户点击视频
            header: false,
            nav: false,
            img: false,
            video: await this.VideoImplService.PalyVideo(params)
        }
    }
}
