const { ipcRenderer } = require('electron');

class Home {
    constructor() {
        console.log(process.argv)
        $(".home").click(()=> {
            // 渲染进程发起ipc，让主进程创建窗体
            ipcRenderer.sendSync('openWindow', ['PlayVideo.controller', true])
        })
    }
}

new Home();
