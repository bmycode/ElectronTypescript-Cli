"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require('module-alias/register');
var electron_1 = require("electron");
var Init_run_1 = require("@run/Init.run");
electron_1.app.on('ready', function () {
    new Init_run_1.Run();
});
electron_1.app.on('window-all-closed', function () {
    if (process.platform !== 'darwin') {
        electron_1.app.quit();
    }
});
//# sourceMappingURL=App.js.map