"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var electron_1 = require("electron");
var Windows_model_1 = __importDefault(require("@/core/model/Windows.model"));
var TouchBarLabel = electron_1.TouchBar.TouchBarLabel, TouchBarButton = electron_1.TouchBar.TouchBarButton, TouchBarSpacer = electron_1.TouchBar.TouchBarSpacer;
var path_1 = require("path");
var Config = /** @class */ (function () {
    function Config() {
    }
    /**
     * 启动页面默认为 Home.controller/Home
     * 程序内部根据这个配置自动载入Home.controller.ts文件，并自动调用类里面的Home()方法实现窗口创建
     * 可以改成其他，例如 PlayVideo.controller/Theme
     */
    Config.StartPage = 'Home.controller/Home';
    // 应用版本号
    Config.CurrentAppVersion = '0.0.1';
    // 是否为Mac系统
    Config.isMac = process.platform === 'darwin';
    Config.LogsPath = "../../../logs/";
    // ajax 请求地址
    Config.ApiUrl = {
        BaseUrl: 'http://www.bmycode.com:3000/api',
        ApiList: {
            PlayList: '/list',
            PalyVideo: '/play',
        }
    };
    // jade 模板引擎配置，更多参数自行阅读声明文件
    Config.jadeCompile0ptions = {
        pretty: true,
        globals: {
            css: [
                'http://mdui-aliyun.cdn.w3cbus.com/source/dist/css/mdui.min.css',
                'http://at.alicdn.com/t/font_1934749_sryayjjvf6.css',
            ],
            js: [
                'http://cdn.bootcdn.net/ajax/libs/jquery/3.5.1/jquery.js',
                'http://mdui-aliyun.cdn.w3cbus.com/source/dist/js/mdui.min.js'
            ]
        }
    };
    // 应用程序的页面地址，地址为打包后的相对路径
    Config.PagePath = {
        Home: path_1.join(__dirname, '../../application/page/Home/Home.html'),
        PlayVideo: path_1.join(__dirname, '../../application/page/PlayVideo/PlayVideo.html'),
    };
    // 所有页面窗体配置
    Config.PageSize = {
        Home: {
            width: 1200,
            height: 760,
            frame: false,
            backgroundColor: '#fff',
            titleBarStyle: 'hiddenInset',
            transparent: true,
            webPreferences: {
                webSecurity: false,
                nodeIntegration: true,
                webviewTag: true,
            }
        },
        PlayVideo: {
            width: 1084,
            height: 610,
            frame: false,
            backgroundColor: '#000',
            titleBarStyle: 'hidden',
            modal: true,
            show: false,
            resizable: true,
            transparent: true,
            webPreferences: {
                webSecurity: false,
                nodeIntegration: true,
                webviewTag: true
            }
        }
    };
    // Mac系统顶部全局菜单 右边图标
    Config.TrayConfig = {
        TopMenuRightImage: path_1.join(__dirname, '../../application/assets/img/pug.png'),
        TopMenuRightDropdown: [
            {
                label: '显示主窗口',
                click: function () {
                    Windows_model_1.default.CurrentBrowserWindow.show();
                }
            },
            {
                icon: path_1.join(__dirname, '../../application/assets/img/pug.png'),
                label: '下拉菜单测试',
                type: 'checkbox',
                checked: true,
                click: function (menuItem, browserWindow) {
                    console.log("menuItem: ", menuItem);
                }
            },
            {
                label: '菜单',
                submenu: [
                    {
                        label: '子菜单1'
                    },
                    {
                        label: '子菜单2'
                    }
                ],
            },
            {
                role: 'quit',
                label: '退出'
            },
        ],
        TopMenuRightTips: '测试提醒'
    };
    // Mac or win 系统顶部全局菜单
    Config.TemplateMenu = [
        {
            label: electron_1.app.name,
            submenu: [
                { label: "\u5173\u4E8E " + electron_1.app.name, role: 'about' },
                { type: 'separator' },
                { label: '服务', role: 'services' },
                { type: 'separator' },
                { label: "\u9690\u85CF " + electron_1.app.name, role: 'hide' },
                { role: 'hideOthers' },
                { label: '隐藏其他', role: 'unhide' },
                { type: 'separator' },
                { label: "\u9000\u51FA" + electron_1.app.name, role: 'quit' }
            ]
        },
        {
            label: '文件',
        },
        {
            label: '编辑',
            submenu: [
                { label: '撤销', role: 'undo' },
                { label: '恢复', role: 'redo' },
                { type: 'separator' },
                { label: '剪切', role: 'cut' },
                { label: '复制', role: 'copy' },
                { label: '粘贴', role: 'paste' },
                { type: 'separator' },
                { label: '粘贴保留样式', role: 'pasteAndMatchStyle' },
                { label: '删除', role: 'delete' },
                { label: '全选', role: 'selectAll' },
                { type: 'separator' },
                {
                    label: '听写',
                    submenu: [
                        { label: '开始听写', role: 'startSpeaking' },
                        { label: '停止听写', role: 'stopSpeaking' }
                    ]
                }
            ]
        },
        {
            label: '视图',
            submenu: [
                { label: '刷新', role: 'reload' },
                { label: '重置', role: 'resetZoom' },
                { label: '放大', role: 'zoomIn' },
                { label: '缩小', role: 'zoomOut' },
                { type: 'separator' },
                { label: '全屏', role: 'togglefullscreen' },
                { label: '切换开发人员工具', role: 'toggleDevTools' },
            ]
        },
        {
            label: '窗口',
            submenu: [
                { label: '最小化', role: 'minimize' },
                { label: '最大化', role: 'zoom' },
                { label: '关闭', role: 'close' }
            ]
        },
        {
            label: '帮助',
            role: 'help',
            submenu: [
                {
                    label: '了解更多',
                    click: function () { return __awaiter(void 0, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4 /*yield*/, electron_1.shell.openExternal('https://electronjs.org')];
                                case 1:
                                    _a.sent();
                                    return [2 /*return*/];
                            }
                        });
                    }); }
                },
                {
                    label: 'GitHub主页',
                    click: function () { return __awaiter(void 0, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            return [2 /*return*/];
                        });
                    }); }
                }
            ]
        }
    ];
    // Mac touchbar 菜单
    Config.TouchBarConfig = {
        items: [
            new TouchBarSpacer({ size: 'small' }),
            new TouchBarButton({
                label: '测试1',
                backgroundColor: '#3a3a3c',
                click: function () { }
            }),
            new TouchBarSpacer({ size: 'small' }),
            new TouchBarButton({
                label: '测试2',
                backgroundColor: '#3a3a3c',
                click: function () { }
            }),
        ]
    };
    return Config;
}());
exports.default = Config;
//# sourceMappingURL=Index.config.js.map