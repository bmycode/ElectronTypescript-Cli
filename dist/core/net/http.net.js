"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Http = void 0;
var Ioc_annotation_1 = require("@annotation/Ioc.annotation");
var logs_utils_1 = require("@utils/logs.utils");
var axios_1 = __importDefault(require("axios"));
var Index_utils_1 = __importDefault(require("@utils/Index.utils"));
var Http = /** @class */ (function () {
    function Http() {
        axios_1.default.defaults.baseURL = Index_utils_1.default.CheckAjaxUrl();
    }
    /**
     * GET请求
     * @param params
     * @constructor
     */
    Http.prototype.GET = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, e_1;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        _a = this;
                        return [4 /*yield*/, axios_1.default.get(params.url, {
                                params: params.data,
                                headers: Object.assign({}, params.header)
                            })];
                    case 1:
                        _a.ResponseData = _b.sent();
                        return [2 /*return*/, this.ResponseData.data];
                    case 2:
                        e_1 = _b.sent();
                        throw new Error("GET \u8BF7\u6C42\u51FA\u9519\uFF1A" + e_1.message);
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * POST请求
     * @param params
     * @constructor
     */
    Http.prototype.POST = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, e_2;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        _a = this;
                        return [4 /*yield*/, axios_1.default.post(params.url, params.data, {
                                headers: Object.assign({}, params.header)
                            })];
                    case 1:
                        _a.ResponseData = _b.sent();
                        return [2 /*return*/, this.ResponseData.data];
                    case 2:
                        e_2 = _b.sent();
                        throw new Error("POST \u8BF7\u6C42\u51FA\u9519\uFF1A" + e_2.message);
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * PUT 请求
     * @param params
     * @constructor
     */
    Http.prototype.PUT = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, e_3;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        _a = this;
                        return [4 /*yield*/, axios_1.default.put(params.url, params.data, {
                                headers: Object.assign({}, params.header)
                            })];
                    case 1:
                        _a.ResponseData = _b.sent();
                        return [2 /*return*/, this.ResponseData.data];
                    case 2:
                        e_3 = _b.sent();
                        throw new Error("PUT \u8BF7\u6C42\u51FA\u9519\uFF1A" + e_3.message);
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * DELETE 请求
     * @param params
     */
    Http.prototype.DELETE = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, e_4;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        _a = this;
                        return [4 /*yield*/, axios_1.default.delete(params.url, {
                                params: params.data,
                                headers: Object.assign({}, params.header)
                            })];
                    case 1:
                        _a.ResponseData = _b.sent();
                        return [2 /*return*/, this.ResponseData.data];
                    case 2:
                        e_4 = _b.sent();
                        throw new Error("DELETE \u8BF7\u6C42\u51FA\u9519\uFF1A" + e_4.message);
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    __decorate([
        Ioc_annotation_1.Inject(),
        __metadata("design:type", logs_utils_1.LogsUtils)
    ], Http.prototype, "LogsUtils", void 0);
    Http = __decorate([
        Ioc_annotation_1.Injectable(),
        __metadata("design:paramtypes", [])
    ], Http);
    return Http;
}());
exports.Http = Http;
//# sourceMappingURL=Http.net.js.map