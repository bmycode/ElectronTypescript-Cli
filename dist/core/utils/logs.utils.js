"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LogsUtils = void 0;
var moment_1 = __importDefault(require("moment"));
var fs_1 = require("fs");
var path_1 = require("path");
var Index_config_1 = __importDefault(require("@config/Index.config"));
var Ioc_annotation_1 = require("@annotation/Ioc.annotation");
var os_1 = __importDefault(require("os"));
var LogsUtils = /** @class */ (function () {
    function LogsUtils() {
    }
    /**
     * 返回日志文件名 application_2020-07-16_21-03-21.log
     * @param level 日志类型
     * @constructor
     */
    LogsUtils.prototype.GetTime = function (level) {
        if (level === void 0) { level = "application"; }
        return level + "_" + moment_1.default().format("YYYY-MM-DD-HH") + ".log";
    };
    /**
     * 传入数据生成日志
     * @param data 数据
     * @param level 日志类型
     */
    LogsUtils.prototype.logs = function (data, level) {
        if (level === void 0) { level = "application"; }
        fs_1.appendFileSync(path_1.join(__dirname, Index_config_1.default.LogsPath + "/" + this.GetTime(level)), "\u3010" + moment_1.default().format("YYYY/MM/DD HH:mm:ss") + "\u3011" + data + os_1.default.EOL + os_1.default.EOL);
    };
    LogsUtils = __decorate([
        Ioc_annotation_1.Injectable()
    ], LogsUtils);
    return LogsUtils;
}());
exports.LogsUtils = LogsUtils;
//# sourceMappingURL=logs.utils.js.map