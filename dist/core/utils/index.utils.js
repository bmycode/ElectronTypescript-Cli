"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var path_1 = require("path");
var Index_config_1 = __importDefault(require("@config/Index.config"));
var electron_1 = require("electron");
var fs_1 = require("fs");
var jade_1 = require("jade");
var Windows_model_1 = __importDefault(require("@model/Windows.model"));
var Utils = /** @class */ (function () {
    function Utils() {
    }
    Utils.CheckAjaxUrl = function () {
        return Index_config_1.default.ApiUrl.BaseUrl;
    };
    /**
     * 返回相对路径
     * @param path 路径
     * @constructor
     */
    Utils.GetFilePath = function (path) {
        return path_1.join(__dirname, path);
    };
    /**
     * 根据配置返回对应的controller类，require后的类
     * @constructor
     */
    Utils.GetController = function () {
        return require("../controller/" + (Index_config_1.default.StartPage.split("/"))[0] + ".js");
    };
    /**
     * Home.controller 拆分为数组，然后controller的首字母C大写
     * 然后返回 HomeController 的类名
     */
    Utils.toUpperCase = function (cont) {
        var Controller = cont.split(".");
        return Controller[0] + Controller[1].charAt(0).toUpperCase() + Controller[1].slice(1);
    };
    /**
     * 系统通知
     * @param parmas
     * @constructor
     */
    Utils.Notification = function (parmas) {
        new electron_1.Notification(parmas).show();
    };
    /**
     * 下载资源
     * @param url 需要下载的资源地址
     * @param path 下载后需要保存的路径
     * @constructor
     */
    Utils.DownFile = function (url, path) {
        Windows_model_1.default.CurrentBrowserWindow.webContents.downloadURL(url);
        Windows_model_1.default.CurrentBrowserWindow.webContents.session.on("will-download", function (event, item, webContents) {
            item.setSavePath(path + "/" + item.getFilename());
            item.once('done', function (event, state) {
                if (state === 'completed') {
                    // 下载成功后显示通知
                    Utils.Notification({
                        title: '下载完成',
                        body: "\u60A8\u7684\u89C6\u9891 " + item.getFilename() + " \u5DF2\u6210\u529F\u4E0B\u8F7D\uFF01",
                        silent: true,
                    });
                }
            });
        });
    };
    /**
     * 创建文件夹
     * @param name 文件夹
     * @param html html
     */
    Utils.mkdir = function (name, html) {
        var path = path_1.join(__dirname, "../../application/page/" + name);
        fs_1.existsSync(path) ? null : fs_1.mkdirSync(path);
        Utils.mkFile(path, name, html);
    };
    /**
     * 生成文件并写入数据
     * @param path 路径
     * @param name html文件名
     * @param html html数据
     */
    Utils.mkFile = function (path, name, html) {
        fs_1.writeFileSync(path + "/" + name + ".html", html);
    };
    /**
     * 创建启动窗口
     * @param target
     * @param name
     */
    Utils.startWindows = function (target, name, params) {
        return __awaiter(this, void 0, void 0, function () {
            var data, Dom;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, target[name](params)];
                    case 1:
                        data = _a.sent(), Dom = jade_1.renderFile(Utils.GetFilePath("../../../src/application/page/" + name + "/" + name + ".jade"), Object.assign(Index_config_1.default.jadeCompile0ptions, data));
                        // 在dist/ 创建并生成对应的html文件
                        Utils.mkdir(name, Dom);
                        try {
                            // @ts-ignore
                            Windows_model_1.default.CurrentBrowserWindow = new electron_1.BrowserWindow(Index_config_1.default.PageSize[name]);
                            // @ts-ignore
                            Windows_model_1.default.CurrentBrowserWindow.loadFile(Index_config_1.default.PagePath[name]).then(function (r) { });
                            Windows_model_1.default.CurrentBrowserWindow.show();
                        }
                        catch (e) {
                            throw new Error("创建窗体失败，请检查配置文件，窗体的html路径是否正确！");
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    return Utils;
}());
exports.default = Utils;
//# sourceMappingURL=Index.utils.js.map