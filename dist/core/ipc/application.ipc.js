"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Application = void 0;
var electron_1 = require("electron");
var Index_utils_1 = __importDefault(require("@utils/Index.utils"));
var Windows_model_1 = __importDefault(require("@model/Windows.model"));
var Index_config_1 = __importDefault(require("@config/Index.config"));
var Application = /** @class */ (function () {
    function Application() {
    }
    /**
     * 创建非主窗体之外的窗体，
     * 在js渲染进程中触发事件
     * 1：独立窗体
     * 2：依附在主窗体上的子窗体
     * 3：依附在独立窗体上的子窗体
     * @constructor
     */
    Application.prototype.CreatedWindow = function () {
        var _this = this;
        electron_1.ipcMain.on('openWindow', function (event, arg) { return __awaiter(_this, void 0, void 0, function () {
            var ControllerName, MethodsName, data, a, a;
            return __generator(this, function (_a) {
                ControllerName = arg.action.split("/")[0], MethodsName = arg.action.split("/")[1], data = arg.data || {};
                // parent false 创建单独的窗体
                if (!arg.parent || !arg.hasOwnProperty("parent")) {
                    a = require("../controller/" + ControllerName + ".js");
                    Index_utils_1.default.startWindows(new a[Index_utils_1.default.toUpperCase(ControllerName)](), MethodsName, data);
                    return [2 /*return*/, false];
                }
                // parent true 创建默认依附主窗口的子窗口
                if (arg.hasOwnProperty("parent") || arg.parent) {
                    // @ts-ignore 合并配置参数，注入 parent: 当前默认启动的父类窗口实例
                    Object.assign(Index_config_1.default.PageSize[MethodsName], { parent: Windows_model_1.default.CurrentBrowserWindow });
                    a = require("../controller/" + ControllerName + ".js");
                    Index_utils_1.default.startWindows(new a[Index_utils_1.default.toUpperCase(ControllerName)](), MethodsName);
                    return [2 /*return*/, false
                        // 创建默认依附在自定义窗口的子窗口
                    ];
                    // 创建默认依附在自定义窗口的子窗口
                }
                else {
                    console.log("创建默认依附在自定义窗口的子窗口");
                }
                return [2 /*return*/];
            });
        }); });
    };
    /**
     * 打开浏览器窗口
     */
    Application.prototype.shellWindows = function () {
        var _this = this;
        electron_1.ipcMain.on("openExternal", function (event, arg) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, electron_1.shell.openExternal(arg.url)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        }); });
    };
    return Application;
}());
exports.Application = Application;
//# sourceMappingURL=Application.ipc.js.map