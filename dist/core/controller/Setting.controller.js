"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SettingController = void 0;
var Creted_annotation_1 = require("@annotation/Creted.annotation");
/**
 * 首页窗体
 * @Render() 注解：
 * 用在类的方法上，注意方法名 和 页面模板名称一致。
 * 作用：表示使用该方法创建页面窗口，方法返回的数据需要是个对象，返回的数据会传给模板
 * 用法：
 * 1：@Render()，不传参数，会根据方法名自动载入对应的jade页面文件来创建窗口
 * 2：@Render('PlayVideo')，传入参数，会根据传入的参数去寻找对应的jade页面来创建窗口
 *    注意：传入参数后不仅会使用自定义页面，还会抛弃配置文件中的 StartPage 主窗窗口配置
 *
 * @Ipc() 注解：
 * 用在类的方法上，接受一个未实例化的类作为参数
 * 作用：Ipc注解会自动运行这个类里面的所有方法，请在方法中放置 ipcMain.on 代码！
 */
var SettingController = /** @class */ (function () {
    function SettingController() {
    }
    SettingController.prototype.Setting = function () {
        return {
            title: '设置页面窗口-测试传给模板的数据',
            desc: '介绍'
        };
    };
    __decorate([
        Creted_annotation_1.Render(),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], SettingController.prototype, "Setting", null);
    return SettingController;
}());
exports.SettingController = SettingController;
//# sourceMappingURL=Setting.controller.js.map