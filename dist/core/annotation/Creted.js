"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Creted = void 0;
const tslib_1 = require("tslib");
const Windows_model_1 = tslib_1.__importDefault(require("@model/Windows.model"));
const electron_1 = require("electron");
const index_config_1 = tslib_1.__importDefault(require("@config/index.config"));
function Creted() {
    return (_constructor) => {
        let ControllerName = _constructor.name.split("Controller")[0];
        let ConfigName = `${ControllerName}Config`;
        let PathName = `${ControllerName}Path`;
        try {
            // @ts-ignore
            Windows_model_1.default.CurrentBrowserWindow = new electron_1.BrowserWindow(index_config_1.default.PageSize[ConfigName]);
            // @ts-ignore
            Windows_model_1.default.CurrentBrowserWindow.loadFile(index_config_1.default.PagePath[PathName]).then(r => { });
        }
        catch (e) {
            console.log("controller不存在，请检查命名，示例：AboutController。");
        }
        Windows_model_1.default.CurrentBrowserWindow.show();
    };
}
exports.Creted = Creted;
