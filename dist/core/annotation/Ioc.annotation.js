"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Inject = exports.Injectable = void 0;
require("reflect-metadata");
var Ioc_model_1 = __importDefault(require("@model/Ioc.model"));
/**
 * 收集类依赖
 * @constructor
 */
function Injectable() {
    return function (_constructor) {
        if (Ioc_model_1.default.classPool.indexOf(_constructor) !== -1) {
            throw new Error('无需重复收集类');
        }
        else {
            //注册
            Ioc_model_1.default.classPool = [_constructor];
        }
    };
}
exports.Injectable = Injectable;
/**
 * 将类依赖实例化然后注入到被装饰的属性中
 * @constructor
 */
function Inject() {
    return function (target, propertyName) {
        /**
         * 使用 reflect-metadata 提供的内置 类型元数据键 design:type 通过反射拿到被装饰属性的类型
         * 也就是 类属性要实例化 的 service 类
         */
        var propertyType = Reflect.getMetadata('design:type', target, propertyName);
        if (Ioc_model_1.default.classPool.indexOf(propertyType) == -1) {
            throw new Error('被装饰的属性所属的变量类型类，没有被装饰器@Injectable()注入，请检查！');
        }
        else {
            // 从存取器的数组中通过下标取出被装饰属性对应的service类，然后实例化这个类，在放入被装饰的属性中
            target[propertyName] = new (Ioc_model_1.default.classPool[Ioc_model_1.default.classPool.indexOf(propertyType)])();
        }
    };
}
exports.Inject = Inject;
//# sourceMappingURL=Ioc.annotation.js.map