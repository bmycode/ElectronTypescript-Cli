"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateTray = exports.CreateTouchbar = exports.CreateApplicationMenu = exports.AutoLoadWindow = void 0;
var Index_utils_1 = __importDefault(require("@utils/Index.utils"));
var ApplicationMenu_interactive_1 = require("@interactive/ApplicationMenu.interactive");
var Touchbar_interactive_1 = require("@interactive/Touchbar.interactive");
var Tray_interactive_1 = require("@interactive/Tray.interactive");
function AutoLoadWindow() {
    return function (_constructor) {
        return /** @class */ (function (_super) {
            __extends(class_1, _super);
            function class_1() {
                var _this = this;
                // 这里只要动态 require 导入类即可，然后类上的装饰器就会运行
                Index_utils_1.default.GetController();
                _this = _super.call(this) || this;
                return _this;
            }
            return class_1;
        }(_constructor));
    };
}
exports.AutoLoadWindow = AutoLoadWindow;
function CreateApplicationMenu() {
    return function (_constructor) {
        return /** @class */ (function (_super) {
            __extends(class_2, _super);
            function class_2() {
                var _this = this;
                // 创建顶部菜单
                new ApplicationMenu_interactive_1.ApplicationMenu();
                _this = _super.call(this) || this;
                return _this;
            }
            return class_2;
        }(_constructor));
    };
}
exports.CreateApplicationMenu = CreateApplicationMenu;
function CreateTouchbar() {
    return function (_constructor) {
        return /** @class */ (function (_super) {
            __extends(class_3, _super);
            function class_3() {
                var _this = this;
                /**
                 * 创建Touchbar
                 * 这边做延时300毫秒的原因解释起来有点复杂
                 * 1：因为我设计了 @Inject() 和 @Injectable() 注解，而在 controller 中
                 *    被注解的属性 VideoImplService 存放的是 请求中间层，而请求中间层是调用的Http请求方法GET，而这个方法
                 *    为了拿到返回值，所以设计成async异步的，这就导致使用请求中间层的controller方法也是async。
                 * 2：而使用的方法如果是 async 那么就会导致 注解 @Render() 内部 也就是 startWindows 方法内，在自动调用方法
                 *    获取传递给模板的方法返回数值的时候必须 await，也就变成了：await target[name]()。
                 *    而这个注解 @CreateTouchbar() 运行时间 和 注解 @AutoLoadWindow() 基本同时间跑，
                 *    所以会导致 @CreateTouchbar() 运行的时候太快了，尼玛币 await target[name]() 还没跑完，所以导致
                 *    存取器里面：Windows.CurrentBrowserWindow 没有数值，所以 @CreateTouchbar() 所依赖的 Windows.CurrentBrowserWindow
                 *    为空，所以touchbar菜单无法 setTouchBar 上。
                 * 3：这里等300毫秒，是等 await target[name]() 完成，窗口也创建完成，然后 Windows.CurrentBrowserWindow
                 *    再去创建 touchbar菜单。
                 *    好了，说完了....同学不要睡了！
                 */
                setTimeout(function () { return new Touchbar_interactive_1.Touchbar(); }, 3000);
                _this = _super.call(this) || this;
                return _this;
            }
            return class_3;
        }(_constructor));
    };
}
exports.CreateTouchbar = CreateTouchbar;
function CreateTray() {
    return function (_constructor) {
        return /** @class */ (function (_super) {
            __extends(class_4, _super);
            function class_4() {
                var _this = this;
                // 创建Mac系统顶部图标
                new Tray_interactive_1.TrayInteractive();
                _this = _super.call(this) || this;
                return _this;
            }
            return class_4;
        }(_constructor));
    };
}
exports.CreateTray = CreateTray;
// export function CreateApplicationIpc(): any {
//     return  (_constructor: {new(...args:any[]):{}}) => {
//         return class extends _constructor {
//             constructor() {
//                 // 开启应用级ipc监听
//                 new Application()
//                 super();
//             }
//         }
//     }
// }
//# sourceMappingURL=Run.annotation.js.map