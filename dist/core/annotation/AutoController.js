"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateApplicationIpc = exports.CreateTray = exports.CreateTouchbar = exports.CreateApplicationMenu = exports.AutoController = void 0;
const tslib_1 = require("tslib");
const index_utils_1 = tslib_1.__importDefault(require("@utils/index.utils"));
const ApplicationMenu_interactive_1 = require("@interactive/ApplicationMenu.interactive");
const Touchbar_interactive_1 = require("@interactive/Touchbar.interactive");
const Tray_interactive_1 = require("@interactive/Tray.interactive");
const application_ipc_1 = require("@ipc/application.ipc");
function AutoController() {
    return (_constructor) => {
        return class extends _constructor {
            constructor() {
                new (index_utils_1.default.GetController());
                super();
            }
        };
    };
}
exports.AutoController = AutoController;
function CreateApplicationMenu() {
    return (_constructor) => {
        return class extends _constructor {
            constructor() {
                // 创建顶部菜单
                new ApplicationMenu_interactive_1.ApplicationMenu();
                super();
            }
        };
    };
}
exports.CreateApplicationMenu = CreateApplicationMenu;
function CreateTouchbar() {
    return (_constructor) => {
        return class extends _constructor {
            constructor() {
                // 创建Touchbar
                new Touchbar_interactive_1.Touchbar();
                super();
            }
        };
    };
}
exports.CreateTouchbar = CreateTouchbar;
function CreateTray() {
    return (_constructor) => {
        return class extends _constructor {
            constructor() {
                // 创建Mac系统顶部图标
                new Tray_interactive_1.TrayInteractive();
                super();
            }
        };
    };
}
exports.CreateTray = CreateTray;
function CreateApplicationIpc() {
    return (_constructor) => {
        return class extends _constructor {
            constructor() {
                // 开启应用级ipc监听
                new application_ipc_1.ApplicationIpc();
                super();
            }
        };
    };
}
exports.CreateApplicationIpc = CreateApplicationIpc;
