"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Windows = /** @class */ (function () {
    function Windows() {
    }
    Object.defineProperty(Windows.prototype, "CurrentWindowNew", {
        get: function () {
            return this._CurrentWindowNew;
        },
        set: function (value) {
            this._CurrentWindowNew = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Windows.prototype, "CurrentBrowserWindow", {
        get: function () {
            return this._CurrentBrowserWindow;
        },
        set: function (value) {
            this._CurrentBrowserWindow = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Windows.prototype, "HomeBrowserWindow", {
        get: function () {
            return this._HomeBrowserWindow;
        },
        set: function (value) {
            this.CurrentBrowserWindow = value;
            this._HomeBrowserWindow = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Windows.prototype, "SettingBrowserWindow", {
        get: function () {
            return this._SettingBrowserWindow;
        },
        set: function (value) {
            this.CurrentBrowserWindow = value;
            this._SettingBrowserWindow = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Windows.prototype, "HomeBrowserWindowWebContents", {
        get: function () {
            return this._HomeBrowserWindowWebContents;
        },
        set: function (value) {
            this._HomeBrowserWindowWebContents = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Windows.prototype, "SettingBrowserWindowWebContents", {
        get: function () {
            return this._SettingBrowserWindowWebContents;
        },
        set: function (value) {
            this._SettingBrowserWindowWebContents = value;
        },
        enumerable: false,
        configurable: true
    });
    return Windows;
}());
exports.default = new Windows();
//# sourceMappingURL=Windows.model.js.map