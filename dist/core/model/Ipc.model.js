"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class IpcModel {
    constructor() {
        this._saveIpcFunction = {};
    }
    get saveIpcFunction() {
        return this._saveIpcFunction;
    }
    set saveIpcFunction(value) {
        this._saveIpcFunction = value;
    }
}
exports.default = new IpcModel();
