"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Run = void 0;
var Run_annotation_1 = require("@annotation/Run.annotation");
var Creted_annotation_1 = require("@annotation/Creted.annotation");
var Application_ipc_1 = require("@ipc/Application.ipc");
/**
 * 启动类
 * 作用：注册全局事件
 * V2版本开发新的注解支持：
 * @AutoLoadWindow() 注解 根据 Index.config.ts 中的 StartPage 自动寻找controller文件夹下的ts文件，然后作为主窗体启动
 * @CreateApplicationMenu() 创建菜单
 * @CreateTouchbar() 创建mac键盘上的Touchbar
 * @CreateTray() 创建mac顶部全局菜单图标
 * @CreateApplicationIpc() 注册全局ipc
 */
var Run = /** @class */ (function () {
    function Run() {
    }
    Run.prototype.event = function () { };
    Run.prototype.ipc = function () { };
    Run.prototype.monitor = function () { };
    Run.prototype.ui = function () { };
    Run = __decorate([
        Run_annotation_1.AutoLoadWindow(),
        Run_annotation_1.CreateApplicationMenu(),
        Run_annotation_1.CreateTouchbar(),
        Run_annotation_1.CreateTray(),
        Creted_annotation_1.CreateApplicationIpc([Application_ipc_1.Application])
    ], Run);
    return Run;
}());
exports.Run = Run;
//# sourceMappingURL=Init.run.js.map