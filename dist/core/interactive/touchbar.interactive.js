"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Touchbar = void 0;
var electron_1 = require("electron");
var Windows_model_1 = __importDefault(require("@/core/model/Windows.model"));
var Index_config_1 = __importDefault(require("@config/Index.config"));
var TouchBarLabel = electron_1.TouchBar.TouchBarLabel, TouchBarButton = electron_1.TouchBar.TouchBarButton, TouchBarSpacer = electron_1.TouchBar.TouchBarSpacer;
var Touchbar = /** @class */ (function () {
    function Touchbar() {
        Windows_model_1.default.CurrentBrowserWindow.setTouchBar(new electron_1.TouchBar(Index_config_1.default.TouchBarConfig));
    }
    return Touchbar;
}());
exports.Touchbar = Touchbar;
//# sourceMappingURL=Touchbar.interactive.js.map