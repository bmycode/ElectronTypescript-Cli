"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TrayInteractive = void 0;
var electron_1 = require("electron");
var Index_config_1 = __importDefault(require("@config/Index.config"));
var TrayInteractive = /** @class */ (function () {
    function TrayInteractive() {
        this.tray = new electron_1.Tray(this.setTemplateImage());
        this.buildTrayMenu();
        this.TrayItemClick();
    }
    /**
     * 全局菜单图标被点击时触发事件
     * @constructor
     */
    TrayInteractive.prototype.TrayItemClick = function () {
        this.tray.on('click', function () {
            console.log("按钮被点击");
        });
    };
    TrayInteractive.prototype.buildTrayMenu = function () {
        var contextMenu = electron_1.Menu.buildFromTemplate(Index_config_1.default.TrayConfig.TopMenuRightDropdown);
        this.tray.setToolTip(Index_config_1.default.TrayConfig.TopMenuRightTips);
        this.tray.setContextMenu(contextMenu);
    };
    /**
     * 创建 NativeImage 图片，图片会随着Mac系统主题的切换而自定变纯黑或纯白
     */
    TrayInteractive.prototype.setTemplateImage = function () {
        var image = electron_1.nativeImage.createFromPath(Index_config_1.default.TrayConfig.TopMenuRightImage);
        image.setTemplateImage(true);
        return image;
    };
    return TrayInteractive;
}());
exports.TrayInteractive = TrayInteractive;
//# sourceMappingURL=Tray.interactive.js.map