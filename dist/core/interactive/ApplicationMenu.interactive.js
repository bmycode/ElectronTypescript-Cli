"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApplicationMenu = void 0;
var electron_1 = require("electron");
var Index_config_1 = __importDefault(require("@config/Index.config"));
var ApplicationMenu = /** @class */ (function () {
    function ApplicationMenu() {
        this.buildFromTemplate();
    }
    ApplicationMenu.prototype.buildFromTemplate = function () {
        this.AppMenu = electron_1.Menu.buildFromTemplate(Index_config_1.default.TemplateMenu);
        electron_1.Menu.setApplicationMenu(this.AppMenu);
    };
    return ApplicationMenu;
}());
exports.ApplicationMenu = ApplicationMenu;
//# sourceMappingURL=ApplicationMenu.interactive.js.map